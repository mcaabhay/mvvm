package abhay.mvvm;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import abhay.mvvm.adapter.SampleAdapter;
import abhay.mvvm.databinding.ActivityMainBinding;
import abhay.mvvm.model.Sample;
import abhay.mvvm.viewmodel.SampleViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity
{
    SampleAdapter adapter;
    CompositeDisposable disposable;
    SampleViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        disposable = new CompositeDisposable();
        final ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        List<Sample> sampleList = new ArrayList<>();
        sampleList.add(new Sample("abhay", "11", "Button 1"));
        sampleList.add(new Sample("ajay", "22", "Button 2"));
        sampleList.add(new Sample("rahul", "55", "Button 5"));
        sampleList.add(new Sample("yogesh", "66", "Button 6"));
        sampleList.add(new Sample("chetan", "44", "Button 4"));
        sampleList.add(new Sample("abhishek", "33", "Button 3"));
        viewModel = new SampleViewModel(sampleList.get(0), this);
        //sorted by name using lambda expressions
        Collections.sort(sampleList, (p1, p2) -> p1.getName().get().compareTo(p2.getName().get()));
        adapter = new SampleAdapter(sampleList);
        binding.recyclerView.setAdapter(adapter);

        //print names starts with a
        printConditionally(sampleList, p -> p.getName().get().startsWith("a"));

        //print all with age above 33
        printConditionally(sampleList, p -> Integer.valueOf(p.getAge()) > 33);

        //print all
        printConditionally(sampleList, p -> true);

        //   Observable<String> myObservable = Observable.just("101");
        //        myObservable.subscribe(new Consumer<String>()
        //        {
        //            @Override
        //            public void accept(
        //                    @NonNull
        //                            String s) throws Exception
        //            {
        //                binding.getMvm().setAge(s);
        //            }
        //        });

    /*    Observable<List<String>> myObservable = Observable.create(new ObservableOnSubscribe<List<String>>()
        {

            @Override
            public void subscribe(ObservableEmitter<List<String>> e) throws Exception
            {
                List<String> sample = new ArrayList<>(2);
                sample.add("Abhay");
                sample.add("30");
                e.onNext(sample);
            }
        });
        myObservable.subscribe(new Consumer<List<String>>()
        {
            @Override
            public void accept(
                    @NonNull
                            List<String> strings) throws Exception
            {
                binding.getMvm().setName(strings.get(0));
                binding.getMvm().setAge(strings.get(1));
            }
        });*/
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //This is for RxJava 2.0
        disposable.add(viewModel.getAgeData()
                                .subscribeOn(Schedulers.computation())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(this::setAgeData));

        //This is for RxJava 1.0
        //        subscription.add(viewModel.getAgeData()
        //                                  .subscribeOn(Schedulers.computation())
        //                                  .observeOn(AndroidSchedulers.mainThread())
        //                                  .subscribe(this::setAgeData));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        disposable.dispose();
    }

    private void setAgeData(String age)
    {
        //Update UI here
    }

    private void printConditionally(List<Sample> samples, Condition condition)
    {
        for(Sample sample : samples)
        {
            if(condition.checkCondition(sample))
            {
                System.out.println(sample);
            }
        }
    }

    //no need to use Condition interface as we already have Predicate interface in Java8 which have test function
    interface Condition
    {
        boolean checkCondition(Sample name);
    }
}
