package abhay.mvvm.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.view.View;
import android.widget.Toast;


import abhay.mvvm.BR;
import abhay.mvvm.model.Sample;
import io.reactivex.Observable;

public class SampleViewModel extends BaseObservable
{
    private Sample sample;
    private Context context;

    public SampleViewModel(Sample sample, Context context)
    {
        this.sample = sample;
        this.context = context;
    }

   /* public String getName()
    {
        return sample.getName();
    }

    public void setName(String name)
    {
        sample.setName(name);
        notifyPropertyChanged(BR.mvm);
    }*/

    public String getAge()
    {
        return sample.getAge();
    }

    public void setAge(String age)
    {
        sample.setAge(age);
        notifyPropertyChanged(BR.mvm);
    }

    public Observable<String> getAgeData()
    {

        return Observable.just(sample.getAge());
    }

    public String getButtonText()
    {
        return sample.getButtonText();
    }

    public void setButtonText(String buttonText)
    {
        sample.setButtonText(buttonText);
    }

    public View.OnClickListener showToastListener()
    {
        return new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                String text = (String) v.getTag();
                Toast.makeText(context, text, Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public String toString()
    {
        return "SampleViewModel{" + "sample=" + sample + '}';
    }
}
