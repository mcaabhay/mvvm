package abhay.mvvm.model;

import android.databinding.BaseObservable;
public class BindableString extends BaseObservable
{
    private String value;

    public static boolean equals(Object a, Object b)
    {
        return (a == null) ? (b == null) : a.equals(b);
    }

    public String get()
    {
        return value != null ? value : "";
    }

    public void set(String value)
    {
        if(!equals(this.value, value))
        {
            this.value = value;
            notifyChange();
        }
    }
}