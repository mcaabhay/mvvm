package abhay.mvvm.model;

import android.databinding.BaseObservable;

public class Sample extends BaseObservable
{
    private BindableString name = new BindableString();
    private String age;
    private String buttonText;

    public Sample(String name, String age, String buttonText)
    {
        this.name.set(name);
        this.age = age;
        this.buttonText = buttonText;
    }

    public BindableString getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }

    public String getButtonText()
    {
        return buttonText;
    }

    public void setButtonText(String buttonText)
    {
        this.buttonText = buttonText;
    }

    @Override
    public String toString()
    {
        return "Sample{" + "name='" + name + '\'' + ", age='" + age + '\'' + ", buttonText='" + buttonText + '\'' + '}';
    }
}
