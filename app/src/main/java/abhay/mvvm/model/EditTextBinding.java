package abhay.mvvm.model;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.widget.EditText;

import abhay.mvvm.R;
/**
 * Created by abhay on 1/5/17.
 */

public class EditTextBinding
{
    @BindingAdapter({"binding"})
    public static void bindEditText(EditText view, final BindableString observableString)
    {
        if(view.getTag(R.id.binded) == null)
        {
            view.setTag(R.id.binded, true);
            view.addTextChangedListener(new TextWatcherAdapter()
            {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                    observableString.set(s.toString());
                }
            });
        }
        String newValue = observableString.get();
        if(!view.getText().toString().equals(newValue))
        {
            view.setText(newValue);
        }
    }

    @BindingConversion
    public static String convertObservableStringToString(BindableString observableString)
    {
        return observableString.get();
    }
}
