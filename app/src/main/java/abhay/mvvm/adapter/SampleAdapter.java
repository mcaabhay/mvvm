package abhay.mvvm.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import abhay.mvvm.R;
import abhay.mvvm.databinding.ListItemBinding;
import abhay.mvvm.model.Sample;

public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.BindingView>
{
    private List<Sample> sampleList = new ArrayList<>();

    public SampleAdapter(List<Sample> sampleList)
    {
        this.sampleList.addAll(sampleList);
    }

    @Override
    public BindingView onCreateViewHolder(ViewGroup parent, int viewType)
    {
        ListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item, parent, false);
        return new BindingView(binding);
    }

    @Override
    public void onBindViewHolder(BindingView holder, int position)
    {
        holder.binding.setMvm(new Sample(sampleList.get(position).getName().get(), sampleList.get(position).getAge(),
                                         sampleList.get(position).getButtonText()));
        holder.binding.button.setTag("Clicked_" + (position + 1));
    }

    @Override
    public int getItemCount()
    {
        return sampleList.size();
    }

    class BindingView extends RecyclerView.ViewHolder
    {
        ListItemBinding binding;

        BindingView(ListItemBinding itemBinding)
        {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }
    }
}
